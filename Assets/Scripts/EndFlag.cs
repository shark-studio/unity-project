﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndFlag : MonoBehaviour {

	public GameObject gameController;


	void OnTriggerEnter2D(Collider2D col) {
		// If the player hits the flag...
		if(col.gameObject.tag == "Player") {
			gameController.GetComponent<EndGame> ().Victory ();
		}
	}


}
