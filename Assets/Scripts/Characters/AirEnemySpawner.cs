﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirEnemySpawner : MonoBehaviour {

	public float minInterval = 1f;
	public float maxInterval = 3f;
	/*public float minX = 1.0f;
	public float maxX = 5.0f;*/
	public Poolable airEnemyPrefab;


	void AirEnemySpawn (){
		GameObject enemyPrefab = airEnemyPrefab.GetInstance();
		enemyPrefab.transform.position = transform.position;
		enemyPrefab.transform.rotation = transform.rotation;
	}

	public IEnumerator SpawnCoroutine(){
		while (true) {
			AirEnemySpawn ();
			yield return new WaitForSeconds(Random.Range(minInterval, maxInterval));
		}


	}

	void Start(){
		StartCoroutine (SpawnCoroutine ());
	}
	
	void OnDisable(){
		StopAllCoroutines ();
	}
}
