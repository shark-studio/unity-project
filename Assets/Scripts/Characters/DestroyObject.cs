﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour {

	/**Trigger used when removing enemies out of the scene
	 * 
	 * */
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Enemy") {
			Poolable poolable = GetComponent<Poolable> ();
			if (!poolable)
				Destroy (col.gameObject);
			else {
				poolable.TryPool ();
			}
		}
	}
}
