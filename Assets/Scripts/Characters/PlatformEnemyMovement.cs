﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformEnemyMovement : MonoBehaviour {
	[SerializeField]
	private float speed=2f;

	public float maxDistance = 3f;
	public LayerMask layerMask;

	private bool shouldReturn;
	private Vector3 forward;


	// Use this for initialization
	void Start () {
		forward = new Vector3 ( 1, -0.5f , 0.0f);
		StartCoroutine(PlatformEnemyMovementCoroutine());

	}
		
	void OnDisable(){
		StopCoroutine (PlatformEnemyMovementCoroutine ());
	}

	void MovementFoward(){
		transform.Translate(new Vector3(1,0,0) * Time.deltaTime * speed, Space.Self);
	}


	public IEnumerator PlatformEnemyMovementCoroutine(){
		while (true) {
			//Offset the ray to make sure it does pick up object of the scenes without interferring with the collider
			Vector3 offsetCurrentPosfoward =  transform.position;
			if (shouldReturn) {
				offsetCurrentPosfoward.x -= 0.5f;
			} else {
				offsetCurrentPosfoward.x += 0.5f;
			}

			Debug.DrawRay (offsetCurrentPosfoward, transform.TransformDirection(forward) * 4, Color.green);

			RaycastHit2D fowardhit = Physics2D.Raycast (offsetCurrentPosfoward, transform.TransformDirection(forward), 4, layerMask);

			if (fowardhit && fowardhit.collider.gameObject.CompareTag ("TurnBack")) {
				shouldReturn = !shouldReturn;
				if (shouldReturn) {
					transform.Rotate (Vector3.up, -180, Space.Self);
						
				} else {
					transform.Rotate (Vector3.up, 180, Space.Self);
				}

			}
			MovementFoward ();
			yield return null;
		}

	}
}
