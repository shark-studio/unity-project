﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightDownwardsMouvement : MonoBehaviour {
	[SerializeField]
	float speed = 5f;

	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up * speed * Time.deltaTime, Space.Self);
	}
}
