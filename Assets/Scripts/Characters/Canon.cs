﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour {

	public Poolable projectilePrefab;

	public float fireInterval = 1f;

	private bool shooting = false;

	/**
	 * Enables or disable the canon to fire bullets( or rockets)
	 * 
	 * */
	public void SetShooting (bool mode){
		shooting = mode;
		StopAllCoroutines ();
		if (mode) {
			StartCoroutine (FireCoroutine ());
		}
	}

	void Fire(){
		GameObject projectile = projectilePrefab.GetInstance ();
		projectile.transform.position = transform.position;
		projectile.transform.rotation = transform.rotation;
	}

	public IEnumerator FireCoroutine(){
		while (true) {
			if (shooting) {
				Fire ();
			}
			yield return new WaitForSeconds (fireInterval);
		}
	}
		
}
