﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirEnemyMovement : MonoBehaviour {

	[SerializeField]
	float speed = 1f;
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	void Move(){
		transform.Translate (Vector3.left * speed * Time.deltaTime, Space.Self);
	}
}
