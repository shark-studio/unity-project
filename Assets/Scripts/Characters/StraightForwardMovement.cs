﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StraightForwardMovement : MonoBehaviour {
	[SerializeField]
	float speed = 5f;


	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.right * speed * Time.deltaTime, Space.Self);
	}


}
