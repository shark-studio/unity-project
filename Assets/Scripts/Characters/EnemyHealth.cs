﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour
{	
	public float health = 100f;					// The player's health.
	public float repeatDamagePeriod = 2f;		// How frequently the player can be damaged.
	public AudioClip ouchClip;				// Array of clips to play when the player is damaged.
	public float damageAmount = 10f;			// The amount of damage to take when player touch the Enemy

	private SpriteRenderer healthBar;			// Reference to the sprite renderer of the health bar.
	private float lastHitTime;					// The time at which the Enemy was last hit.
	private Vector3 healthScale;				// The local scale of the health bar initially (with full health).

	void Awake ()
	{
		// Setting up references.
		healthBar = GameObject.Find("HealthBarEnemy").GetComponent<SpriteRenderer>();

		// Getting the intial scale of the healthbar (whilst the player has full health).
		healthScale = healthBar.transform.localScale;
	}


	void OnCollisionEnter2D (Collision2D col)
	{
		// If the colliding gameobject is an Player...
		if(col.gameObject.tag != "Enemy")
		{

			// ... and if the time exceeds the time of the last hit plus the time between hits...
			if (Time.time > lastHitTime + repeatDamagePeriod) 
			{
				// ... and if the player still has health...
				if(health > 0f)
				{
					// ... take damage and reset the lastHitTime.
					TakeDamage(col.transform); 
					lastHitTime = Time.time; 
				}
				// If the Enemy doesn't have health, do some stuff, let him fall into the river to reload the level.
				else
				{
					// Find all of the colliders on the gameobject and set them all to be triggers.
					Collider2D[] cols = GetComponents<Collider2D>();
					foreach(Collider2D c in cols)
					{
						c.isTrigger = true;
					}

					// Move all sprite parts of the Enemt to the front
					SpriteRenderer[] spr = GetComponentsInChildren<SpriteRenderer>();
					foreach(SpriteRenderer s in spr)
					{
						s.sortingLayerName = "UI";
					}
					// ... disable user Player Control script
					GetComponent<PlatformEnemyMovement>().enabled = false;

					Poolable p = gameObject.GetComponent<Poolable> ();
					if (!p)
						Destroy (gameObject);
					else{
						p.TryPool ();
					}

					GameObject obj = GameObject.Find ("UI_HealthDisplayEnemy");
					if (obj) {
						obj.SetActive (false);
					}

					Score score = GameObject.Find ("Score").GetComponent<Score>();
					score.score += 100;

					// ... disable the Gun script to stop a dead guy shooting a nonexistant bazooka
					//GetComponentInChildren<Pl>().enabled = false;

				}
			}
		}
	}


	void TakeDamage (Transform player)
	{
		// Reduce the player's health by 10.
		health -= damageAmount;

		// Update what the health bar looks like.
		UpdateHealthBar();

		// Play the clip of the player getting hurt.
		AudioSource.PlayClipAtPoint(ouchClip, transform.position);
	}


	public void UpdateHealthBar ()
	{
		// Set the health bar's colour to proportion of the way between green and red based on the Enemy's health.
		healthBar.material.color = Color.Lerp(Color.green, Color.red, 1 - health * 0.01f);

		// Set the scale of the health bar to be proportional to the Enemy's health.
		healthBar.transform.localScale = new Vector3(healthScale.x * health * 0.01f, 1, 1);
	}
}
