﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletMovement : MonoBehaviour {

	public string targetTag = "Player";
	public float rotationSpeed = 10f;

	private GameObject target;

	void OnEnable(){
		if (!target) {
			target = GameObject.FindGameObjectWithTag (targetTag);
		}
	}

	// Update is called once per frame
	void Update () {
		if(target)
			Move ();
	}

	/**
	 * Rotation of the bullet depending on the position of the player
	 * 
	 * */
	void Move(){

		Vector3 directionToTarget = target.transform.position - transform.position;

		float angleToTarget = Vector3.Angle (transform.up, directionToTarget);

		Vector3 rotationAxis = Vector3.Cross (transform.up, directionToTarget);

		angleToTarget = Mathf.Clamp (rotationSpeed * Time.deltaTime, rotationSpeed * Time.deltaTime, angleToTarget);

		transform.Rotate(rotationAxis, angleToTarget);


	}
}
