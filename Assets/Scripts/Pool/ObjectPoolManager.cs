﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : Singleton<ObjectPoolManager> {

	private Dictionary<string, ObjectPool> pools = new Dictionary<string, ObjectPool> ();

	ObjectPool GetPool(GameObject prefab){
		ObjectPool pool;
		if (!pools.TryGetValue (prefab.name, out pool)) {
			pool = new ObjectPool (prefab);
			pools.Add (prefab.name, pool);
		}
		return pool;
	}

	public GameObject GetObject(GameObject prefab){
		return GetPool (prefab).getObject ();
	}

	public bool PoolObject(GameObject prefab){
		return GetPool (prefab).poolObject (prefab);
	}
	
}
