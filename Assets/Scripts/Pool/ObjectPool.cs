﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool  {
	
	public Queue<GameObject> pool = new Queue<GameObject>();

	public GameObject prefab;

	public ObjectPool(GameObject prefab){
		this.prefab = prefab;
	}

	public GameObject getObject(){
		GameObject obj;

		if (pool.Count == 0) {
			obj = Object.Instantiate<GameObject> (prefab);
			obj.name = prefab.name;
		} else {
			obj = pool.Dequeue ();
		}

		return obj;
	}

	public bool poolObject(GameObject obj){
		if (obj.name != prefab.name) {
			return false;
		}

		pool.Enqueue (obj);
		return true;
	}
}
