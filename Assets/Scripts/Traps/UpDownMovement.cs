﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownMovement : MonoBehaviour {

	[SerializeField]
	private float speed=2f;
	public float maxHeight = 0f;

	private Vector3 startPos;
	private Vector3 targetPos;


	void Start () {

		startPos = gameObject.transform.position;
		targetPos = new Vector3 (startPos.x, maxHeight, startPos.z);
		StartCoroutine(PicsMovementCoroutine());
	}


	void OnDisable(){
		StopCoroutine (PicsMovementCoroutine ());
	}

	void Movement(){
		transform.position = ( Vector3.Lerp (startPos, targetPos, Mathf.PingPong (Time.time *speed ,  0.5f)));

	}

	public IEnumerator PicsMovementCoroutine(){
		while (true) {
			Movement ();
			yield return null;
		}


	}






}
