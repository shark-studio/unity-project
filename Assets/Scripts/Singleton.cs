﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T:Singleton<T> {

	private static T instance;

	public static T Instance{
		get{
			if (!instance) {
				instance = FindObjectOfType<T> ();

				if (!instance) {
					Debug.LogError ("There needs to be at least one instance of" + typeof(T) + " on the scene");
				}
			}
			return instance;
		}
	}


	void Awake(){
		if (instance) {
			Destroy (gameObject);
		} else {
			instance = (T)this;
			DontDestroyOnLoad (this.gameObject);
		}
	}
}
