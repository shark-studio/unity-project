﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingFollowingPlayer : MonoBehaviour {
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
	}
}
