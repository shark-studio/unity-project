﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {
	public int HP = 3;					// How many times the enemy can be hit before it dies.

	private SpriteRenderer ren;			// Reference to the sprite renderer. (to change it maybe depending HP)
	private bool destroyed = false;		// Whether or not the enemy is dead.


	void Awake() {
		// Setting up the references.
		ren = GetComponent<SpriteRenderer>();
	}

	void FixedUpdate () {
		// If the obstacle has zero or fewer hit points and isn't destroyed yet...
		if(HP <= 0 && !destroyed)
			// ... call the destroy function.
			Smash ();
	}


	public void Hurt() {
		// Reduce the number of hit points by one.
		HP--;
	}


	void Smash() {
		destroyed = true;
		Destroy (gameObject);

	}


}
