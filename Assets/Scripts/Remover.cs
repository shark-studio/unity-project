﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Remover : MonoBehaviour {
	public GameObject splash;

	private bool _triggered;


	void OnTriggerEnter2D(Collider2D col) {
		// If the player hits the trigger...
		if(col.gameObject.tag == "Player" && !_triggered) {
			_triggered = true;
			// .. stop the camera tracking the player
			GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ScrollingFollowingPlayer>().enabled = false;

			// .. stop the Health Bar following the player
			GameObject healthBar = GameObject.FindGameObjectWithTag("HealthBar");
			if(healthBar != null && healthBar.activeSelf) {
				GameObject.FindGameObjectWithTag("HealthBar").SetActive(false);
			}

			// ... instantiate the splash where the player falls in.
			Instantiate(splash, col.transform.position, transform.rotation);

			col.gameObject.GetComponent<PlayerHealth>().LooseLife();	
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if (!_triggered) {
			return;
		}
		_triggered = false;
	}


}
