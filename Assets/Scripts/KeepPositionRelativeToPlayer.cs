﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepPositionRelativeToPlayer : MonoBehaviour {
	private GameObject player;
	private float distanceX;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		distanceX = gameObject.transform.position.x - player.transform.position.x;
	}

	// Update is called once per frame
	void Update () {
		transform.position = transform.position = new Vector3(player.transform.position.x+distanceX, transform.position.y, transform.position.z);
	}
}
