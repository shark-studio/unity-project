﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lave : MonoBehaviour {

	private bool _triggered;

	void OnTriggerEnter2D (Collider2D col){
		if (col.gameObject.tag == "Player" && !_triggered) {
			_triggered = true;
			col.gameObject.GetComponent<PlayerHealth> ().LooseLife ();
		}
	}


	void OnTriggerExit2D(Collider2D col){
		if (!_triggered) {
			return;
		}
		_triggered = false;
	}

}