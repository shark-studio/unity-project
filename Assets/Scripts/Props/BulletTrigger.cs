﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTrigger : MonoBehaviour {

	public GameObject explosion;		// Prefab of explosion effect.


	void OnExplode() {
		// Create a quaternion with a random rotation in the z-axis.
		Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));

		// Instantiate the explosion where the rocket is with the random rotation.
		Instantiate(explosion, transform.position, randomRotation);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag != "Coin") {
			OnExplode ();
			Poolable poolable = GetComponentInParent<Poolable> ();
			if (poolable) {
				poolable.TryPool ();
			} else {
				Destroy (gameObject);
			}
		}
	}


}