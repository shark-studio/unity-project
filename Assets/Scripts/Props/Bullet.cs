using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public GameObject explosion;		// Prefab of explosion effect.


	void Start () {
	}


	void OnExplode() {
		// Create a quaternion with a random rotation in the z-axis.
		Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));

		// Instantiate the explosion where the rocket is with the random rotation.
		Instantiate(explosion, transform.position, randomRotation);
	}


	void OnCollisionEnter2D (Collision2D col) {
		// Call the explosion instantiation.
		OnExplode();
		Poolable poolable = GetComponent<Poolable> ();
		if (poolable) {
			poolable.TryPool ();
		} else {
			Destroy (gameObject);
		}
	}


}
