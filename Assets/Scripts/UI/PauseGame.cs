﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {
	public GameObject pauseMenu;

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Pause();
		}
	}


	public void Pause() {
		if (pauseMenu.activeInHierarchy == false) {
			Time.timeScale = 0;
			pauseMenu.SetActive(true);
		} else {
			pauseMenu.SetActive(false);
			Time.timeScale = 1;
		}
	}


}
