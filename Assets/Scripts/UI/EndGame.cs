﻿using UnityEngine;
using System.Collections;

public class EndGame : MonoBehaviour {
	public GameObject congratulationsPanel;
	public GameObject gameOverPanel;


	public void Victory() {
		gameObject.GetComponent<PauseGame> ().enabled = false;
		congratulationsPanel.SetActive (true);
		Time.timeScale = 0;
	}


	public void GameOver() {
		gameObject.GetComponent<PauseGame> ().enabled = false;
		gameOverPanel.SetActive (true);
		Time.timeScale = 0;
	}


}
