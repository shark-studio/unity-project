﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {
	public int numberOfScenes = 3;

	public void LoadByIndex(int sceneIndex) {
		Time.timeScale = 1;
		SceneManager.LoadScene (sceneIndex);
	}

	public void RestartCurrentScene() {
		Time.timeScale = 1;
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
	}

	public void LoadNextScene() {
		Time.timeScale = 1;
		if (SceneManager.GetActiveScene ().buildIndex == numberOfScenes) {
			SceneManager.LoadScene (0);
		} else {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		}
	}

}