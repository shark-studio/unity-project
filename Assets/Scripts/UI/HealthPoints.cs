﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthPoints : MonoBehaviour {
	public GameObject player;

	private int hp;


	void Awake () {
		hp = player.GetComponent<PlayerHealth> ().hp;
		GetComponent<Text>().text = "Life: " + hp;
	}


	void Update () {
		hp = player.GetComponent<PlayerHealth> ().hp;
		GetComponent<Text>().text = "Life: " + hp;
	}


}
