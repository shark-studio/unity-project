﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScore : MonoBehaviour {
	public GameObject scoreUI;

	private int score;


	// Use this for initialization
	void Start () {
		score = scoreUI.GetComponent<Score> ().score;
		GetComponent<Text> ().text = "   Score : "+score.ToString();
	}
	
	void OnEnable() {
		score = scoreUI.GetComponent<Score> ().score;
		GetComponent<Text> ().text = "   Score : "+score.ToString();
	}


}
